Rails.application.routes.draw do
  resources :presences
  resources :students
  resources :lessons
  resources :teachers
  resources :professors
  resources :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
