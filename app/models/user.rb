class User < ApplicationRecord
    has_one :teacher

    enum kind: {
        Aluno: 0,
        Professor: 1,
        admin: 2
    }
end
