class Presence < ApplicationRecord
  belongs_to :student
  belongs_to :lesson

  enum situation: {
    Presente:0,
    Ausente: 1
  }
end
