class Teacher < ApplicationRecord
  belongs_to :user
  has_many :lessons

  def name
    user.name
  end

end
