class Student < ApplicationRecord
  validates :name, presence:true
  belongs_to :user
  has_many :lessons

  def name
    user.name
  end 
end
