json.extract! teacher, :id, :subject, :salary, :user_id, :created_at, :updated_at
json.url teacher_url(teacher, format: :json)
